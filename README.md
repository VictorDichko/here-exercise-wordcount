# Wordcount exercise

This project contains implementation of word count 
from `CharacterReader` interface.

## Getting Started

### Part 1

The implementation is based on the following trait:
```scala
trait WordCounter {
  def countWords(characterReader: CharacterReader): Future[TreeSet[WordCount]]
}
```

Class `WordCounterImpl` contains concrete implementation

Example of usage:
```scala
object WordCounterApp extends App {

  implicit val system: ActorSystem = ActorSystem("word-counter")
  implicit val dispatcher: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val log: LoggingAdapter = Logging(system, getClass)

  val wordCounter = new WordCounterImpl()

  val characterReader = new FastCharacterReaderImpl()

  wordCounter.countWords(characterReader)
    .map(result => log.info("Final Total Result:\n" + formatted(result)))
    .onComplete(_ => system.terminate())
}
```

To run `WordCounterApp`, execute:
```sbtshell
sbt "runMain com.advancedtelematic.interview.wordcount.WordCounterApp"
```

### Part 2

The implementation is based on the following trait:
```scala
trait WordCounterSlowParallel {
  def countWords(characterReaders: Seq[CharacterReader], interval: FiniteDuration)(implicit log: LoggingAdapter): Future[TreeSet[WordCount]]
}
```

The interval when combined counts are logged is set in config:
```hocon
wordcount {
  output-interval = 10 seconds
}
```
Example of usage:
```scala
object WordCounterSlowParallelApp extends App {

  implicit val system: ActorSystem = ActorSystem("word-counter-slow-parallel")
  implicit val dispatcher: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val log: LoggingAdapter = Logging(system, getClass)

  val config = ConfigFactory.load().getConfig("wordcount")
  val interval = FiniteDuration(config.getDuration("output-interval", TimeUnit.SECONDS), TimeUnit.SECONDS)

  val wordCounter = new WordCounterSlowParallelImpl()

  val characterReaders = Seq.fill(10)(new SlowCharacterReaderImpl())

  wordCounter.countWords(characterReaders, interval)
    .map(result => log.info("Final Total Result:\n" + formatted(result)))
    .onComplete(_ => system.terminate())
}
```
To run `WordCounterSlowParallelApp`, execute:
```sbtshell
sbt "runMain com.advancedtelematic.interview.wordcount.WordCounterSlowParallelApp"
```

## Running the tests

Execute tests using:
```sbtshell
sbt test
```
