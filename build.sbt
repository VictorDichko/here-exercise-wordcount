name := "here-exercise-wordcount"

version := "1.0"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % "2.5.14",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.14" % Test,
  "org.scalatest" %% "scalatest" % "3.0.5" % Test
)
