package com.advancedtelematic.interview.wordcount

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.stream.ActorMaterializer
import com.advancedtelematic.interview.wordcount.Stages._

import scala.collection.immutable.TreeSet
import scala.concurrent.{ExecutionContext, Future}


trait WordCounter {

  def countWords(characterReader: CharacterReader): Future[TreeSet[WordCount]]

}

class WordCounterImpl(implicit mat: ActorMaterializer, log: LoggingAdapter) extends WordCounter {

  override def countWords(characterReader: CharacterReader): Future[TreeSet[WordCount]] = {
    sourceFromCharacterReaderLogged(characterReader)
      .via(charsToWordsFlow)
      .runWith(wordCountSinkSorted)
  }
}

object WordCounterApp extends App {

  implicit val system: ActorSystem = ActorSystem("word-counter")
  implicit val dispatcher: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val log: LoggingAdapter = Logging(system, getClass)

  val wordCounter = new WordCounterImpl()

  val characterReader = new FastCharacterReaderImpl()

  wordCounter.countWords(characterReader)
    .map(result => log.info("Final Total Result:\n" + formatted(result)))
    .onComplete(_ => system.terminate())

}
