package com.advancedtelematic.interview.wordcount

import java.io.EOFException

import akka.event.LoggingAdapter
import akka.stream.FlowShape
import akka.stream.scaladsl.{Flow, Framing, GraphDSL, Partition, Sink, Source}
import akka.util.ByteString
import akka.{Done, NotUsed}

import scala.collection.immutable.TreeSet
import scala.concurrent.Future
import scala.concurrent.duration.{FiniteDuration, _}
import scala.language.postfixOps
import scala.util.control.NonFatal

object Stages {

  private def nextCharacterOptEither(reader: CharacterReader)(implicit log: LoggingAdapter): Option[Either[Throwable, Char]] = {
    try {
      Some(Right(reader.nextCharacter()))
    } catch {
      case _: EOFException => None
      case NonFatal(ex) => Some(Left(ex))
    }
  }

  private def sourceFromCharacterReaderEither(characterReader: CharacterReader)(implicit log: LoggingAdapter): Source[Either[Throwable, Char], NotUsed] = {
    Source.unfoldResource[Either[Throwable, Char], CharacterReader](
      () => characterReader,
      reader => nextCharacterOptEither(reader),
      reader => reader.close()
    )
  }

  def sourceFromCharacterReaderLogged(characterReader: CharacterReader)(implicit log: LoggingAdapter): Source[Char, NotUsed] = {
    sourceFromCharacterReaderEither(characterReader).via(errorsSplitter)
  }

  private def errorsSplitter(implicit log: LoggingAdapter): Flow[Either[Throwable, Char], Char, NotUsed] = {
    Flow.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val partition = b.add(Partition[Either[Throwable, Char]](2, either => if (either.isRight) 1 else 0))
      val flow = b.add(Flow[Char])

      partition.out(0).map(_.left.get) ~> errorsSink
      partition.out(1).map(_.right.get) ~> flow

      FlowShape(partition.in, flow.out)
    })
  }

  private def errorsSink(implicit log: LoggingAdapter): Sink[Throwable, Future[Done]] =
    Sink.foreach[Throwable](error => log.error(error, "Exception occurred"))

  private val charToByteStringMapper: Flow[Char, ByteString, NotUsed] =
    Flow[Char].map(c => if (c == '\n') ByteString(' ') else ByteString(c))

  private val byteStringToWordsMapper: Flow[ByteString, String, NotUsed] =
    Flow[ByteString]
      .via(Framing.delimiter(ByteString(" "), maximumFrameLength = Int.MaxValue, allowTruncation = true))
      .map(_.utf8String)

  private val wordsNormalizer: Flow[String, String, NotUsed] =
    Flow[String].map(word => word.toLowerCase.replaceAll("""[\p{Punct}]""", ""))

  val charsToWordsFlow: Flow[Char, String, NotUsed] =
    Flow[Char]
      .via(charToByteStringMapper)
      .via(byteStringToWordsMapper)
      .via(wordsNormalizer)

  def intervalTicksSource(interval: FiniteDuration): Source[Boolean, NotUsed] = {
    val tickSource = Source.tick(initialDelay = 0 seconds, interval, tick = true)
    Source.repeat(element = false).merge(tickSource)
  }

  private def addWordToSet(currentSet: TreeSet[WordCount], word: String): TreeSet[WordCount] = {
    val count = currentSet.find(_.word == word).map(_.count).getOrElse(0)
    currentSet - WordCount(word, count) + WordCount(word, count + 1)
  }

  val wordCountSinkSorted: Sink[String, Future[TreeSet[WordCount]]] =
    Sink.fold(TreeSet.empty[WordCount]) { (acc: TreeSet[WordCount], word: String) =>
      addWordToSet(acc, word)
    }

  def wordCountSinkWithLogs(implicit log: LoggingAdapter): Sink[(String, Boolean), Future[TreeSet[WordCount]]] =
    Sink.fold(TreeSet.empty[WordCount]) { (acc: TreeSet[WordCount], wordTick: (String, Boolean)) =>
      val (word, tick) = wordTick
      val updatedSet = addWordToSet(acc, word)
      if (tick) {
        log.info(s"Current word count:\n" + formatted(updatedSet))
      }
      updatedSet
    }
}
