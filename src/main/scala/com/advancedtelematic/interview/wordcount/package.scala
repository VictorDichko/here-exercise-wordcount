package com.advancedtelematic.interview

import scala.collection.immutable.TreeSet

package object wordcount {

  case class WordCount(word: String, count: Int)

  implicit def orderingWordCount[A <: WordCount]: Ordering[A] = Ordering.by(wc => (-wc.count, wc.word))

  def formatted(treeSet: TreeSet[WordCount]): String = {
    treeSet.toSeq.map(wordCount => wordCount.word + " - " + wordCount.count).mkString("\n")
  }

}
