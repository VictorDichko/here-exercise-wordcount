package com.advancedtelematic.interview.wordcount

import java.util.concurrent.TimeUnit

import akka.NotUsed
import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.stream.scaladsl.{GraphDSL, Merge, Source, Zip}
import akka.stream.{ActorMaterializer, SourceShape}
import com.advancedtelematic.interview.wordcount.Stages.{charsToWordsFlow, sourceFromCharacterReaderLogged, wordCountSinkWithLogs, intervalTicksSource}
import com.typesafe.config.ConfigFactory

import scala.collection.immutable.TreeSet
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps


trait WordCounterSlowParallel {

  def countWords(characterReaders: Seq[CharacterReader], interval: FiniteDuration)(implicit log: LoggingAdapter): Future[TreeSet[WordCount]]

}

class WordCounterSlowParallelImpl(implicit mat: ActorMaterializer, log: LoggingAdapter) extends WordCounterSlowParallel {

  private def mergedSources(characterReaders: Seq[CharacterReader], interval: FiniteDuration): Source[(String, Boolean), NotUsed] = {
    Source.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._
      val numSources = characterReaders.size

      val merge = b.add(Merge[String](numSources))

      for (characterReader <- characterReaders) {
        sourceFromCharacterReaderLogged(characterReader) ~> charsToWordsFlow ~> merge
      }

      val zip = b.add(Zip[String, Boolean])

      merge ~> zip.in0
      intervalTicksSource(interval) ~> zip.in1

      SourceShape(zip.out)
    })
  }

  override def countWords(characterReaders: Seq[CharacterReader], interval: FiniteDuration)(implicit log: LoggingAdapter): Future[TreeSet[WordCount]] = {
    mergedSources(characterReaders, interval).runWith(wordCountSinkWithLogs)
  }

}

object WordCounterSlowParallelApp extends App {

  implicit val system: ActorSystem = ActorSystem("word-counter-slow-parallel")
  implicit val dispatcher: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val log: LoggingAdapter = Logging(system, getClass)

  val config = ConfigFactory.load().getConfig("wordcount")
  val interval = FiniteDuration(config.getDuration("output-interval", TimeUnit.SECONDS), TimeUnit.SECONDS)

  val wordCounter = new WordCounterSlowParallelImpl()

  val characterReaders = Seq.fill(10)(new SlowCharacterReaderImpl())

  wordCounter.countWords(characterReaders, interval)
    .map(result => log.info("Final Total Result:\n" + formatted(result)))
    .onComplete(_ => system.terminate())

}
