package com.advancedtelematic.interview.wordcount

import java.io.EOFException
import java.util.Random
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Keep
import akka.stream.testkit.scaladsl.TestSource
import akka.testkit.{TestKit, TestProbe}
import com.advancedtelematic.interview.wordcount.Stages.wordCountSinkWithLogs
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.{AsyncWordSpecLike, BeforeAndAfterAll, MustMatchers}

import scala.collection.immutable.TreeSet
import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps

class WordCounterSlowParallelTest extends TestKit(ActorSystem("word-counter-slow-parallel-system"))
  with AsyncWordSpecLike
  with BeforeAndAfterAll
  with MustMatchers {

  override def afterAll: Unit = {
    system.terminate()
  }

  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val log: LoggingAdapter = Logging(system, getClass)

  val config: Config = ConfigFactory.load().getConfig("wordcount")
  val interval = FiniteDuration(config.getDuration("output-interval", TimeUnit.SECONDS), TimeUnit.SECONDS)

  class SlowCharacterReaderTestImpl(val data: String) extends CharacterReader {
    val dataChars: mutable.Queue[Char] = mutable.Queue(data.toSeq: _*)
    val random = new Random()

    override def nextCharacter(): Char = {
      Thread.sleep(random.nextInt(10))
      if (dataChars.nonEmpty) dataChars.dequeue() else throw new EOFException
    }

    override def close(): Unit = ()
  }


  "WordCounterSlowParallel" should {

    "count words from 10 streams" in {

      val slowReaders = Seq(
        new SlowCharacterReaderTestImpl("OTA Plus is a trusted solution for simultaneous updates"),
        new SlowCharacterReaderTestImpl("of the entire ecosystem of ECUs inside a vehicle,"),
        new SlowCharacterReaderTestImpl("ensuring ECUs are always current, as well as intercompatible."),
        new SlowCharacterReaderTestImpl("A versatile solution for OEMs,"),
        new SlowCharacterReaderTestImpl("OTA Plus delivers many different kinds of software and other updates,"),
        new SlowCharacterReaderTestImpl("including delta-compressed binaries, full disk image updates,"),
        new SlowCharacterReaderTestImpl("map packs, and firmware install packages with special install instructions."),
        new SlowCharacterReaderTestImpl("With an accurate, up-to-date database of the installed software across an entire fleet,"),
        new SlowCharacterReaderTestImpl("OTA Plus makes it quick and easy for OEMs to determine their fleet's exposure in the event of a new 0-day vulnerability,"),
        new SlowCharacterReaderTestImpl("allowing a rapid, targeted campaign to deploy a fix.")
      )

      val expectedTuples = Seq(("a", 6), ("of", 5), ("and", 3), ("for", 3), ("ota", 3), ("plus", 3), ("the", 3),
        ("updates", 3), ("an", 2), ("as", 2), ("ecus", 2), ("entire", 2), ("install", 2), ("oems", 2), ("software", 2),
        ("solution", 2), ("to", 2), ("with", 2), ("0day", 1), ("accurate", 1), ("across", 1), ("allowing", 1),
        ("always", 1), ("are", 1), ("binaries", 1), ("campaign", 1), ("current", 1), ("database", 1), ("delivers", 1),
        ("deltacompressed", 1), ("deploy", 1), ("determine", 1), ("different", 1), ("disk", 1), ("easy", 1),
        ("ecosystem", 1), ("ensuring", 1), ("event", 1), ("exposure", 1), ("firmware", 1), ("fix", 1), ("fleet", 1),
        ("fleets", 1), ("full", 1), ("image", 1), ("in", 1), ("including", 1), ("inside", 1), ("installed", 1),
        ("instructions", 1), ("intercompatible", 1), ("is", 1), ("it", 1), ("kinds", 1), ("makes", 1), ("many", 1),
        ("map", 1), ("new", 1), ("other", 1), ("packages", 1), ("packs", 1), ("quick", 1), ("rapid", 1),
        ("simultaneous", 1), ("special", 1), ("targeted", 1), ("their", 1), ("trusted", 1), ("uptodate", 1),
        ("vehicle", 1), ("versatile", 1), ("vulnerability", 1), ("well", 1))
      val expectedTreeSet = TreeSet(expectedTuples.map(WordCount.tupled): _*)

      val wordCounter = new WordCounterSlowParallelImpl()
      val future = wordCounter.countWords(slowReaders, interval)
      future.map { result =>
        result mustBe expectedTreeSet
      }
    }

    "log current result on tick" in {
      val logProbe: TestProbe = {
        val probe = TestProbe()
        system.eventStream.subscribe(probe.ref, classOf[Logging.LogEvent])
        probe
      }
      val logSrc = s"${getClass.getSimpleName}(akka://${system.name})"
      val logClazz = classOf[WordCounterSlowParallelTest]

      TestSource.probe[(String, Boolean)]
        .toMat(wordCountSinkWithLogs)(Keep.left)
        .run()
        .sendNext(("hello", false))
        .sendNext(("world", true))
        .sendNext(("hello", false))
        .sendNext(("universe", true))

      val result1 = TreeSet(WordCount("hello", 1), WordCount("world", 1))
      val result2 = TreeSet(WordCount("hello", 2), WordCount("universe", 1), WordCount("world", 1))

      logProbe.expectMsg(Logging.Info(logSrc, logClazz, "Current word count:\n" + formatted(result1)))
      logProbe.expectMsg(Logging.Info(logSrc, logClazz, "Current word count:\n" + formatted(result2)))

      succeed
    }
  }
} 