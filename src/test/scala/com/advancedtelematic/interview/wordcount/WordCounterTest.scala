package com.advancedtelematic.interview.wordcount

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import org.scalatest.{AsyncWordSpecLike, BeforeAndAfterAll, MustMatchers}

import scala.collection.immutable.TreeSet
import scala.language.postfixOps

class WordCounterTest extends TestKit(ActorSystem("word-counter-system"))
  with AsyncWordSpecLike
  with BeforeAndAfterAll
  with MustMatchers {

  override def afterAll: Unit = {
    system.terminate()
  }

  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val log: LoggingAdapter = Logging(system, getClass)

  "WordCounter" should {
    "count words from empty stream" in {
      val characterReader = new FastCharacterReaderImpl("")
      val wordCounter = new WordCounterImpl()

      val future = wordCounter.countWords(characterReader)
      future.map { result =>
        result mustBe TreeSet.empty[WordCount]
      }
    }

    "count words from sentences with punctuation" in {
      val testData = "For this new world to work for everyone, we believe the infrastructure should be based " +
        "on an open data platform instead of in corporate-owned walled gardens.\n" +
        "Your data should always remain your data, and the cost of entry should not be a mandatory free transfer of ownership."

      val expectedTuples = Seq(("data", 3), ("of", 3), ("should", 3), ("be", 2), ("for", 2), ("the", 2), ("your", 2),
        ("a", 1), ("always", 1), ("an", 1), ("and", 1), ("based", 1), ("believe", 1), ("corporateowned", 1), ("cost", 1),
        ("entry", 1), ("everyone", 1), ("free", 1), ("gardens", 1), ("in", 1), ("infrastructure", 1), ("instead", 1),
        ("mandatory", 1), ("new", 1), ("not", 1), ("on", 1), ("open", 1), ("ownership", 1), ("platform", 1),
        ("remain", 1), ("this", 1), ("to", 1), ("transfer", 1), ("walled", 1), ("we", 1), ("work", 1), ("world", 1))
      val expectedTreeSet = TreeSet(expectedTuples.map(WordCount.tupled): _*)

      val characterReader = new FastCharacterReaderImpl(testData)
      val wordCounter = new WordCounterImpl()

      val future = wordCounter.countWords(characterReader)
      future.map { result =>
        result mustBe expectedTreeSet
      }
    }
  }
} 