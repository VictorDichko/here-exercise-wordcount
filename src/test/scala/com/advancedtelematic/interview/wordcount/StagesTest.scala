package com.advancedtelematic.interview.wordcount

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestKit
import com.advancedtelematic.interview.wordcount.Stages._
import org.scalatest._

import scala.collection.immutable.TreeSet
import scala.language.postfixOps

class StagesTest extends TestKit(ActorSystem("stages-system"))
  with AsyncWordSpecLike
  with BeforeAndAfterAll
  with MustMatchers {

  override def afterAll: Unit = {
    system.terminate()
  }

  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val log: LoggingAdapter = Logging(system, getClass)

  "sourceFromCharacterReader" should {
    "create source from CharacterReader with no content" in {
      val fastCharacterReader = new FastCharacterReaderImpl("")
      val future = sourceFromCharacterReaderLogged(fastCharacterReader).runWith(Sink.seq)
      future.map { result =>
        result mustBe empty
      }
    }

    "create source from CharacterReader with some content" in {
      val testData = "Hello, here!"
      val fastCharacterReader = new FastCharacterReaderImpl(testData)
      val future = sourceFromCharacterReaderLogged(fastCharacterReader).runWith(Sink.seq)
      future.map { result =>
        result mustBe testData.toSeq
      }
    }
  }

  "charsToWordsFlow" should {
    "read single word from CharacterReader" in {
      val testData = "hello"
      val fastCharacterReader = new FastCharacterReaderImpl(testData)
      val future = sourceFromCharacterReaderLogged(fastCharacterReader).via(charsToWordsFlow).runWith(Sink.seq)
      future.map { result =>
        result mustBe Seq(testData)
      }
    }

    "read multiple words from CharacterReader" in {
      val testData = "hello world"
      val fastCharacterReader = new FastCharacterReaderImpl(testData)
      val future = sourceFromCharacterReaderLogged(fastCharacterReader).via(charsToWordsFlow).runWith(Sink.seq)
      future.map { result =>
        result mustBe Seq("hello", "world")
      }
    }

    "read a sentence with punctuation from CharacterReader" in {
      val testData = "Hello, world!"
      val fastCharacterReader = new FastCharacterReaderImpl(testData)
      val future = sourceFromCharacterReaderLogged(fastCharacterReader).via(charsToWordsFlow).runWith(Sink.seq)
      future.map { result =>
        result mustBe Seq("hello", "world")
      }
    }

    "read few sentences with punctuation from CharacterReader" in {
      val testData = "Hello, world!\n" + "World, hello!"
      val fastCharacterReader = new FastCharacterReaderImpl(testData)
      val future = sourceFromCharacterReaderLogged(fastCharacterReader).via(charsToWordsFlow).runWith(Sink.seq)
      future.map { result =>
        result mustBe Seq("hello", "world", "world", "hello")
      }
    }
  }

  "wordCountSinkSorted" should {
    "count 0 words" in {
      val source = Source.empty
      val future = source.runWith(wordCountSinkSorted)
      future.map { result =>
        result mustBe TreeSet.empty[WordCount]
      }
    }

    "count 1 word" in {
      val source = Source(List("word"))
      val future = source.runWith(wordCountSinkSorted)
      future.map { result =>
        result mustBe TreeSet(WordCount("word", 1))
      }
    }

    "count multiple words each 1 time" in {
      val source = Source(List("hello", "world"))
      val future = source.runWith(wordCountSinkSorted)
      future.map { result =>
        result mustBe TreeSet(WordCount("hello", 1), WordCount("world", 1))
      }
    }

    "count multiple words multiple times" in {
      val source = Source(List("hello", "world", "hello", "world"))
      val future = source.runWith(wordCountSinkSorted)
      future.map { result =>
        result mustBe TreeSet(WordCount("hello", 2), WordCount("world", 2))
      }
    }

    "sort by count and words" in {
      val source = Source(List("the", "cat", "sat", "on", "the", "mat"))
      val resultSet = TreeSet(
        WordCount("the", 2),
        WordCount("cat", 1),
        WordCount("mat", 1),
        WordCount("on", 1),
        WordCount("sat", 1)
      )
      val future = source.runWith(wordCountSinkSorted)
      future.map { result =>
        result mustBe resultSet
      }
    }
  }
} 